# Demo code for an Unix Socket FD-passing server

The code is built and set up for cross compilation to x86_64, musl and armv7,
as well as wrapped in a container for deploy usage on modio devices.


This code is meant to run with dbus, if you don't have a bus running (most
Linuxes do, the ones that don't should know about it), use:

    dbus-run-session -- cargo run


There is a companion repo in <https://gitlab.com/ModioAB/dbus_fd_client> which
has a client to connect and read data from the socket.


Build with the usual  `cargo build`  and run with `cargo run`

# What it does?

It connects to dbus on a well known service name, and exposes an interface that
returns a file-descriptor to a unix datagram socket.

For each client that connects, it spawns a new task and socketpair and writes a
message every 100ms. There is little error-handling, as tasks are backgrounded
and not polled for status.


# Extras

There is a simple command-line handler to provide a `--help` flag, because the
CI system for contained applications expect that to work and return success, as
a test to ensure that a packaged service has all dependencies it need to at
least start.

There's a basic systemd service, and a config-file, for the same reason.

Design wise the CI system expects the service name to match the binary, and the
config file, ie `dbus-fd-server.conf`, `dbus-fd-server.service` and so on.
