FROM --platform=linux/arm registry.gitlab.com/modioab/scada-hello/template:armhf

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown
ARG TARGET=armv7-unknown-linux-gnueabihf

LABEL "se.modio.ci.url"=$URL "se.modio.ci.branch"=$BRANCH "se.modio.ci.commit"=$COMMIT "se.modio.ci.host"=$HOST "se.modio.ci.date"=$DATE "se.modio.service"=dbus-fd-server

COPY config/hosts /etc/hosts
COPY config/resolv.conf /etc/resolv.conf
COPY config/machine-id  /etc/machine-id
COPY config/dbus-fd-server.conf /etc/dbus-fd-server.conf
COPY config/dbus-fd-server.service /etc/systemd/system/dbus-fd-server.service

COPY target/${TARGET}/release/dbus-fd-server /opt/bin/dbus-fd-server
