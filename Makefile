TARGET ?= armv7-unknown-linux-gnueabihf

IMAGE_REPO = registry.gitlab.com/modioab/dbus-fd-server
IMAGE_FILES += target/$(TARGET)/release/dbus-fd-server
IMAGE_FILES += config/dbus-fd-server.service
IMAGE_FILES += config/dbus-fd-server.conf
IMAGE_FILES += config/hosts
IMAGE_FILES += config/machine-id
IMAGE_FILES += config/resolv.conf

CLEANUP_FILES += config/hosts 
CLEANUP_FILES += config/machine-id
CLEANUP_FILES += config/resolv.conf

include build.mk


config/hosts config/machine-id config/resolv.conf:
	touch $@
