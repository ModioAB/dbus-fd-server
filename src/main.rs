use argh::FromArgs;
use async_std::stream::StreamExt;
use async_std::task;
use serde::Serialize;
use serde_json::to_vec;
use std::os::fd::IntoRawFd;
use std::os::unix::net::UnixDatagram;
use std::time::Duration;
use std::time::{SystemTime, UNIX_EPOCH};
use zbus::zvariant::Type;

#[derive(FromArgs, Debug)]
/// Command line basics
///
struct CommandLine {
    /// use session bus
    ///
    /// By default we use the DBus System bus, add this flag to use the Session bus, or "user" bus
    /// instead. Mainly useful when developing
    #[argh(switch)]
    _session: bool,

    /// verbosity
    ///
    /// Add more -v -v -v in order to get a higher log level (not implemented)
    #[argh(switch, short = 'v')]
    _verbose: u32,
}

// How often we read. also used for time-outs etc.
const RATE: Duration = Duration::from_millis(100);

#[derive(Debug, Type, Serialize)]
struct Payload {
    time: SystemTime,
    demo_value: f64,
}

impl Default for Payload {
    fn default() -> Payload {
        let millis = match SystemTime::now().duration_since(UNIX_EPOCH) {
            Ok(v) => v.as_millis(),
            Err(_) => 0,
        };
        Payload {
            time: SystemTime::now(),
            demo_value: millis as f64,
        }
    }
}
#[derive(Debug)]
struct BgTask {
    writer: async_std::os::unix::net::UnixDatagram,
    interval: async_std::stream::Interval,
}

async fn run_bgtask(mut task: BgTask) -> anyhow::Result<()> {
    println!("Spawned new bg task: {task:?}");
    while let Some(_) = task.interval.next().await {
        let payload = Payload::default();
        let encoded = to_vec(&payload)?;
        task.writer.send(&encoded).await?;
        println!("Wrote one msg");
    }
    println!("Reader side has failed us: {task:?}");
    Ok(())
}

#[derive(Default)]
struct FdTest {
    tasks: Vec<async_std::task::JoinHandle<anyhow::Result<()>>>,
}

#[zbus::dbus_interface(name = "se.modio.FdTest")]
impl FdTest {
    pub async fn get_reader(&mut self) -> zbus::fdo::Result<zbus::zvariant::Fd> {
        let (reader, writer) =
            UnixDatagram::pair().expect("Failed to create new socket. out of file handles?");
        writer
            .set_read_timeout(Some(RATE))
            .expect("Failed to set read timeout");
        writer
            .set_write_timeout(Some(RATE))
            .expect("Failed to set write timeout");

        let writer: async_std::os::unix::net::UnixDatagram = writer.into();

        let interval = async_std::stream::interval(RATE);
        let bg_task = BgTask { writer, interval };
        let task = async_std::task::spawn(run_bgtask(bg_task));
        // If we want to keep track of readers coming and going, we should do something here
        self.tasks.push(task);
        //
        let fd = reader.into_raw_fd().into();
        dbg!(&fd);
        println!("We currently have {:?} bg tasks", &self.tasks);
        Ok(fd)
    }
}

#[async_std::main]
async fn main() -> anyhow::Result<()> {
    let commandline: CommandLine = argh::from_env();
    println!("CommandLine: {:#?}", commandline);

    let fdtest = FdTest::default();

    let _server_conn = zbus::ConnectionBuilder::session()?
        .name("se.modio.FdTest")?
        .serve_at("/se/modio/fdtest", fdtest)?
        .build()
        .await?;

    loop {
        println!("Sleeeping");
        task::sleep(Duration::from_secs(60)).await;
    }
}
